import hashlib, re, os
import tornado.ioloop
import tornado.web
import tornado.autoreload
import tornado.template
from pymongo import MongoClient
import redis
from sockjs.tornado import SockJSRouter, SockJSConnection
import sclhub.redis_request_handler
import sclhub.tools.formvalidator
import sclhub.tools.uploader
from sclhub.handlers import wsocks, index, register, auth, messages, wall, profile, uploader, search, news

tpl_loader = tornado.template.Loader("./tmpl")
mongo_client = MongoClient()
redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)
wsock = SockJSRouter(wsocks.WSocksHandler, '/wsock', dict(user=""))
msg_server = SockJSRouter(wsocks.MessagesHandler, '/msg_server', dict(user=""))
settings = {"debug":True, "login_url": "/"}

application = tornado.web.Application([
    (r"/", index.IndexHandler, dict(redis=redis_client, loader=tpl_loader, mongo=mongo_client)),
    (r'/static/(.*)', tornado.web.StaticFileHandler, {"path": "./static"}),
    (r"/register", auth.RegisterHandler, dict(redis=redis_client, mongo=mongo_client)),
    (r"/confirm", auth.RegisterHandler, dict(redis=redis_client, mongo=mongo_client)),
    (r"/login", auth.LoginHandler, dict(loader=tpl_loader, mongo=mongo_client, redis=redis_client)),
    (r"/dialogs", messages.DialogsHandler, dict(loader=tpl_loader, mongo=mongo_client, redis=redis_client)),
    (r"/talks", messages.MessagesHandler, dict(loader=tpl_loader, mongo=mongo_client, redis=redis_client)),
    (r"/search", search.SearchHandler, dict(loader=tpl_loader, mongo=mongo_client, redis=redis_client)),
    (r"/profile/([a-zA-Z_0-9]+)", index.ProfileHandler, dict(loader=tpl_loader, mongo=mongo_client, redis=redis_client)),
    (r"/news", news.NewsHandler, dict(loader=tpl_loader, mongo=mongo_client, redis=redis_client)),
    # (r"/notifications", notifs.NotifsHandler, dict(loader=tpl_loader, mongo=mongo_client, redis=redis_client)),
    #AJAX
    (r"/api/v1/messages/([a-zA-Z_0-9]+)", messages.DialogsHandler, dict(loader=tpl_loader, mongo=mongo_client, redis=redis_client)),
    (r"/api/v1/posts/([a-zA-Z_0-9]+)", wall.BlogHandler, dict(mongo=mongo_client, redis=redis_client)),
    (r"/api/v1/comments/([a-zA-Z_0-9]+)", wall.CommentHandler, dict(mongo=mongo_client, redis=redis_client)),
    (r"/api/v1/like/([a-zA-Z_0-9]+)", wall.LikeHandler, dict(mongo=mongo_client, redis=redis_client)),
    (r"/api/v1/reblog/([a-zA-Z_0-9]+)", wall.ReblogHandler, dict(mongo=mongo_client, redis=redis_client)),
    (r"/api/v1/friendship/([a-zA-Z_0-9]+)", wall.FriendHandler, dict(mongo=mongo_client, redis=redis_client)),
    (r"^/api/v1/upload/image/(temp)?(avatar)?(perm)?$", uploader.ImageUploaderHandler, dict(redis=redis_client, mongo=mongo_client)),
    (r"/api/v1/user/update", auth.RegisterHandler, dict(redis=redis_client, mongo=mongo_client)),
    (r"/api/v1/password", auth.PasswordManager, dict(redis=redis_client, mongo=mongo_client)),
    (r"/api/v1/profile", auth.ProfileInfoManager, dict(redis=redis_client, mongo=mongo_client)),
    (r"/api/v1/users/([a-zA-Z_0-9]+)", auth.UserManager, dict(redis=redis_client, mongo=mongo_client)),
    (r"/api/v1/search/(.{1,50})", search.SearchHandler, dict(loader=tpl_loader, redis=redis_client, mongo=mongo_client)),
    #Users
    (r"/([a-zA-Z_0-9]+)", profile.ProfileHandler, dict(redis=redis_client, loader=tpl_loader, mongo=mongo_client)),
] + wsock.urls + msg_server.urls, **settings)

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()