function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined 
}

function compile_post(update){
    return '\
                    <div class="post col-lg-12 clearfix" pid="' + update._id + '">\
                <img src="'+ update.pic +'" alt="' + update.author + '" height="50px" class="pull-left">\
                <div class="pull-left post-body col-lg-10">\
                    <a href="#">' + update.author + '</a> <span>' + update.date + '</span>\
                    <i class="pull-right icon-trash delete-post" pid="' + update._id + '"></i><div class="post-content">'
                        + update.content +
                    '</div>\
                    <div class="post-info">\
                        <span class="pull-left">Добавить ответ</span>\
                        <a href="#"><i class="icon-heart like pull-right">0</i></a>\
                        <a href="#"><i class="icon-retweet reblog pull-right">0</i></a>\
                    </div>\
            <div class="post-comments">\
                <hr>\
                <div class="input-group add-comment">\
                  <input type="text" class="form-control comment-content" placeholder="Введите текст комментария">\
                  <span class="input-group-btn">\
                    <button class="btn btn-primary comment-send" pid="' + update._id + '" type="button"><i class="icon-angle-right"></i></button>\
                  </span>\
                </div>\
            </div>';
}

function compile_com(update){
    return '<div class="media" cid="'+ update._id +'">\
                            <a class="pull-left" href="'+ update.author +'"><img class="media-object" height="35px" src="'+ update.pic +'"></a>\
                            <div class="media-body">\
                                <a class="media-heading" href="#">' + update.author + '<span class="pull-right delete-comment" comment_id="' + update.comment_id + '">×</span></a>'
                            + update.content +'</div>\
                            <hr>\
                        </div>'
}

$(document).ready(function(){

	//Bootstrap
	$(".tooltips").tooltip();
    $('.navbar .badge').popover({
        html:true,
        title: 'Уведомления<a href="#" class="pull-right">Все</a>',
        // content: "У вас пока нет уведомлений"
        content:'<div class="media">\
  <a class="pull-left" href="#">\
    <img src="/static/img/sheldon-square.jpg" alt="Sheldon Cooper" height="36px">\
  </a>\
  <div class="media-body">\
  Это какое-то содержимое уведомления\
  </div>\
</div>'
    });
    //Updates
    var sock = new SockJS('http://localhost:8888/wsock');
    var pipe = {
        type : "subscribition",
        channel_type : $("body").attr("update"),
        channel_id : $("body").attr("owner")
    };
    pipe = JSON.stringify(pipe);
    //Connection
    sock.onopen = function(){
        sock.send(pipe);
    };
    // sock.send(message);
    sock.onmessage = function(update){
        update = JSON.parse(update.data);
        console.log(update);
        if (update.type == "add_post"){
            $('.posts').prepend('\
                    <div class="post col-lg-12 clearfix" pid="' + update._id + '">\
                <img src="'+ update.pic +'" alt="' + update.author + '" height="50px" class="pull-left">\
                <div class="pull-left post-body col-lg-10">\
                    <a href="#">' + update.author + '</a> <span>' + update.date + '</span>\
                    <i class="pull-right icon-trash delete-post" pid="' + update._id + '"></i><div class="post-content">'
                        + update.content +
                    '</div>\
                    <div class="post-info">\
                        <span class="pull-left">Добавить ответ</span>\
                        <a href="#"><i class="icon-heart like pull-right">0</i></a>\
                        <a href="#"><i class="icon-retweet reblog pull-right">0</i></a>\
                    </div>\
            <div class="post-comments">\
                <hr>\
                <div class="input-group add-comment">\
                  <input type="text" class="form-control comment-content" placeholder="Введите текст комментария">\
                  <span class="input-group-btn">\
                    <button class="btn btn-primary comment-send" pid="' + update._id + '" type="button"><i class="icon-angle-right"></i></button>\
                  </span>\
                </div>\
            </div>');
        }else if (update.type == "delete_post"){
            $("div[pid='" + update.pid + "']").remove();
        }else if (update.type == "add_comment"){
            $(".post[pid='" + update.pid + "'] .add-comment").before(compile_com(update));
        }else if (update.type == "delete_comment"){
            $("div[cid='" + update.cid + "']").remove();
        }else if (update.type == "like"){
            var like = $(".post[pid='" + update.pid + "'] .like");
            if (update.act == "-1"){
                like.html(parseInt(like.html(), 10)-1);
                like.removeClass("liked");
            }
            else if (update.act == "1"){
                like.html(parseInt(like.html(), 10)+1);
                like.addClass("liked");
            }
        }else if (update.type == "reblog"){
            var reblog = $(".post[pid='" + update.pid + "'] .reblog");
            if (update.act == "-1"){
                reblog.html(parseInt(reblog.html(), 10)-1);
                reblog.removeClass("rebloged");
            } else if (update.act == "1"){
                reblog.html(parseInt(reblog.html(), 10)+1);
                reblog.addClass("rebloged");
            }
        }else if (update.type == "load_posts"){
            $.each(update.posts, function(index, post){
                $(".posts").append(compile_post(post));
                console.log(post);
                });
        }else if (update.type == "load_coms"){
            $.each(update.comments, function(index, com){
                $(".post[pid='"+ com.pid +"'] .add-comment").before(compile_com(com));
                });
        }else if (update.type == "error"){
            $("#info-box").append(html_err(update.error))
        }
    };
    sock.onclose = function(){
        sock = null;
    };
    sock.onerror = function(){
        sock.close()
    };

	//System
    wall_owner = $("body").attr("owner");

	$(document).on('click', '.post-info span:first-child', function(){
		var comments = $(this).parents(".post-body").children(".post-comments");
        if ($(this).attr("com_count") < "5" || $(this).attr("loaded") == "true"){
            if (comments.css("display") == "none"){
                if ($(this).attr("com_count") != "0")
                    $(this).html("Скрыть ответы");
                comments.show();
            }else{
                if ($(this).attr("com_count") != "0")
                    $(this).html("Показать ответы");
                comments.hide();
            }
        }else if ($(this).attr("loaded") != "true"){
            $(this).attr("loaded", "true");
            data = {
                type:"load_coms",
                pid : $(this).parents(".post").attr("pid"),
                count : $(this).parents(".post-body").children(".post-comments").children("div").length - 1,
            }
            sock.send(JSON.stringify(data));
        }
	});
	$(document).on('click', '.msg-send',function(){
		data = {
			content: $(".msg-form > textarea").val(),
			owner : $(".msg-form > textarea").attr("user"),
            type:"add_post",
            skey:getCookie("session_data")
		};
        sock.send(JSON.stringify(data));
	});
	$(document).on('click', '.delete-post', function(){
		post = $(this);
        data = {
            pid:$(this).attr("pid"),
            type:"del_post",
            skey:getCookie("session_data")
        };
        sock.send(JSON.stringify(data));
	});
    $(document).on('click', '.comment-send', function(){
        data = {
            content: $(this).parents(".media-body").children("textarea").val(),
            pid: $(this).attr("pid"),
            type:"add_comment",
            skey:getCookie("session_data")
        };
        sock.send(JSON.stringify(data));
    });
    $('.media-body textarea').keydown(function (e) {
    if (e.ctrlKey && e.keyCode == 13) {
        data = {
            content: $(this).val(),
            pid: $(this).parents(".media-body").children(".comment-send").attr("pid"),
            type:"add_comment",
            skey:getCookie("session_data")
        };
        sock.send(JSON.stringify(data));
    }});
    $(document).on('click', '.delete-comment', function(){
        comment = $(this);
        data = {
            cid:$(this).parents(".media").attr("cid"),
            type:"del_comment",
            pid:$(this).parents(".post").attr("pid"),
            skey:getCookie("session_data"),
        }
        sock.send(JSON.stringify(data));
    });
    $(document).on('click', '.like', function(){
        like = $(this);
        data = {
            pid:like.parents(".post").attr("pid"),
            type:"like",
            skey:getCookie("session_data")
        }
        sock.send(JSON.stringify(data));
    });
    $(document).on('click', '.reblog', function(){
        reblog = $(this);
        data = {
            pid:reblog.parents(".post").attr("pid"),
            type:"reblog",
            skey:getCookie("session_data")
        }
        sock.send(JSON.stringify(data));
    });
    $("#friendship").click(function(){
        var status = $(this).attr("friendship_status");
        var link = $(this);
        if (status == 0){
            $.post("/api/v1/friendship/" + wall_owner, '', function(){
                link.attr("friendship_status", 1);
                link.html("Удалить заявку в друзья");
            });
        }else if (status == 1){
            $.ajax({
                url: "/api/v1/friendship/" + wall_owner,
                type: 'PUT',
                success: function() {
                    link.attr("friendship_status", 0);
                    link.html("Отправить заявку в друзья");
            }});
        }
        else if (status == 2){
            $.ajax({
                url: "/api/v1/friendship/" + wall_owner,
                type: 'DELETE',
                success: function() {
                    link.attr("friendship_status", 0);
                    link.html("Отправить заявку в друзья");
            }});
        }
    });

    $("#load_posts").on('click', function() {
        data = {
            type:"load_posts",
            count:$(".posts > div").length,
            page:"personal",
            owner:$("#load_posts").attr("owner")
        }
        sock.send(JSON.stringify(data));
    });

    $(".add-comment textarea").keyup(function()
    {
        var maxcount = 180;
        var box=$(this).val();
        var main = box.length *100;
        var value= (main / maxcount);
        var count= maxcount - box.length;

        if(box.length <= maxcount)
        {
            console.log(count);
            $(this).parents(".media").children('a').children('.com-counter').html(count);
        }
        return false;
    });
});