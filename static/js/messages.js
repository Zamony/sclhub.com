function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined 
}

function html_msg(data){
    return '<span href="#" class="list-group-item">\
            <img height="40px" src="' + data["author"]["pic"] + '" class="pull-left">\
            <h5 class="list-group-item-heading">\
            <a href="/' + data["msg_author"] + '">' + data["author"]["full_name"] + '</a></h5>\
            <p class="list-group-item-text">' + data["msg_content"] + '</p>\
            </span>';
}

function html_more(){
    return '<span class="list-group-item" id="more">\
            <div class="btn btn-default col-lg-12">Загрузить больше сообщений</div>\
            </span>';
}

$(document).ready(function(){
   $(".dialog-bg, .dialog").height($(window).height() - ($(".msg-form").height() + $(".navbar").height() + 6*parseInt($(".navbar").css("margin-bottom"))));
   $('.dialog').mousewheel(function(event, delta, deltaX, deltaY) {
      var scrollTop = $(this).scrollTop();
      $(this).scrollTop(scrollTop-Math.round(delta * 20));
   });
   $("#messages").animate({ scrollTop: $(document).height() }, "fast");
      var sock = new SockJS('http://localhost:8888/msg_server');
      msg = {
         type:"subscribe_one",
         skey:getCookie("session_data"),
         dialog_id:$("textarea").attr("dialog_id")
      }
      msg = JSON.stringify(msg);
      sock.onopen = function() {
         console.log('opened');
         sock.send(msg);
      };
      sock.onmessage = function(update) {
         msg = JSON.parse(update.data);
         console.log(msg);
         if (msg.type == "msg"){
            $("#messages").append(html_msg(msg));
            //$("#messages").animate({ scrollTop: $(document).height() }, "slow");
         } else if (msg.type == "dialog_update"){
            $("#dialogs a[dialog_id='" + msg["dialog_id"] + "'] p").html(msg["msg_content"]);
         } else if (msg.type == "more"){
            $.each(msg.messages, function(k, i){
              //console.log(k, i["msg_content"])
              $("#messages > span:first-child").before(html_msg(i));
            });
            if (msg.more)
              $("#messages > span:first-child").before(html_more());
         } else if (msg.type == "error"){
            $("#info-box").append(html_err(msg.error))
         }
      };
      sock.onclose = function(msg) {
         console.log('closed');
         console.log(msg);
      };
      sock.onerror = function(err){
         console.log(err);
      };
  $("#new_talk").on("click", function(){
      $("#new_message").modal();
  });
  $("#msg-send").click(function(){
      var msg = {
          content : $(this).parents(".modal-dialog").find("textarea").val(),
          receivers: $(this).parents(".modal-dialog").find("select").val(),
      }
      $.post("/api/v1/messages/send" , msg, function(){
          window.location.reload();
      });
  });
  $("#more").on("click", function(){
    var msgs_count = $("#messages > span").length
    if ($("#messages > span:first-child").attr("id") == "more")
      --msgs_count;
     msg = {
        "type":"more",
        "skey":getCookie("session_data"),
        "dialog_id":$("textarea").attr("dialog_id"),
        "count": msgs_count
     }
     console.log(msg);
     msg = JSON.stringify(msg);
     if (msg) sock.send(msg);
     $(this).remove();
  });
  $('#msg-sector textarea').keydown(function (e) {
  if (e.ctrlKey && e.keyCode == 13) {
       msg = {
          "type":"push",
          "skey":getCookie("session_data"),
          "content":$("textarea").val(),
          "receivers":$.trim($("textarea").attr("receivers")).split(" "),
          "dialog_id":$("textarea").attr("dialog_id")
       }
       console.log(msg);
       msg = JSON.stringify(msg);
       if (msg) sock.send(msg);
  }
  });
});