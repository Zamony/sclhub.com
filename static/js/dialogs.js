function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined 
}

$(document).ready(function(){
    var sock = new SockJS('http://localhost:8888/msg_server');
    var pipe = {
        type : "subscribe_all",
        skey : getCookie("session_data")
    };
    pipe = JSON.stringify(pipe);
    //Connection
    sock.onopen = function(){
        sock.send(pipe);
    };
    // sock.send(message);
    sock.onmessage = function(update){
        update = JSON.parse(update.data);
        console.log(update);
    };
    sock.onclose = function(){
        sock = null;
    };
    sock.onerror = function(){
        sock.close()
    };
    $("#msg-send").click(function(){
        var msg = {
            content : $(this).parents(".modal-dialog").find("textarea").val(),
            receivers: $(this).parents(".modal-dialog").find("select").val(),
        }
        $.post("/api/v1/messages/send" , msg, function(){});
    });

    $(".dialog-item").click(function(){
        window.location.href = "/talks?did=" + $(this).attr("dialog_id");
    });
});