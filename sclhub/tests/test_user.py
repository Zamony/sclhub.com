import unittest
import redis
from pymongo import MongoClient
from tools.user import User, UserError

mongo_client = MongoClient()
redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)

class TestUser(unittest.TestCase):
    username = "zamony"

    def test_online_friends(self):
        # self.user = User(self.username, redis_client, mongo_client)
        # self.assertEquals(self.user.online_friends, [])
        # self.assertEquals(self.user.profile["friends"], [])
        # self.username = "new_user"
        pass
        # self.user = User(self.username, redis_client, mongo_client)
        # self.assertEquals(self.user.online_friends, [])
        # self.assertEquals(self.user.profile["friends"], [])
    def test_update_profile(self):
        pass
    def test_verify_data(self):
        self.user = User(self.username, redis_client, mongo_client)
        data = {}
        data["email"] = "ya@ya.rt"
        self.user._verify_data(data)
        data["email"] = "as89.a@a.com.ua"
        self.user._verify_data(data)
        data["email"] = "as89..a@om.ua"
        data["full_name"] = "Its Test"
        data["password"] = "dewqds"
        self.user._verify_data(data)


if __name__ == "__main__":
    unittest.main()