import unittest
import redis
from bson import ObjectId
from pymongo import MongoClient
import wand.image
from tools.blog import Post, PostError, Comment, CommentError, Tape, TapeError
from tools.user import User


class MockPostsMongo(object):
    posts = {
    "5298dbe156c153264f3ea920":dict(
        content="Qwerty",
        author="zamony",
        _id=ObjectId("5298dbe156c153264f3ea920"),
        likes=["zamony"],
        owner="zamony",
        reblogs=[]
        )
    }

    def find_one(self, iid):
        return self.posts[str(iid["_id"])]

    def insert(self, content):
        print(content)
        # return "5298dbe156c153264f3ea122"

    def remove(self, content):
        print(content)
        # return "5298dbe156c153264f3ea654"

    def update(self, content, up):
        print(content, up)


class MockCommentsMongo(object):
    coms = {
    "5298dbe156c153264f3ea920":dict(
        content="Qwerty",
        author="zamony",
        _id=ObjectId("5298dbe156c153264f3ea920"),
        )
    }

    def find_one(self, iid):
        return self.posts[str(iid["_id"])]

    def insert(self, content):
        print(content)
        # return "5298dbe156c153264f3ea122"

    def remove(self, content):
        print(content)
        # return "5298dbe156c153264f3ea654"

    def update(self, content, up):
        print(content, up)

class MockUsersMongoZamony(object):
    profile = dict(
    username="zamony",
    friends=["sheldon"],
    )

class MockUsersMongoSheldon(object):
    profile = dict(
    username="sheldon",
    friends=["zamony"],
    )

posts_mongo = type('test_mongo', (object,),
            {'users': type('test_collection', (object,),
            {'walls': MockPostsMongo()})})

comments_mongo = type('test_mongo', (object,),
            {'users': type('test_collection', (object,),
            {'comments': MockCommentsMongo()})})

class TestPost(unittest.TestCase):

    def test_valid(self):
        try:
            popt = dict(
                # pid="5298dbe156c153264f3ea920",
                c_user=MockUsersMongoZamony(),
                mongo=posts_mongo,
                post=dict(
                    content="Пост",
                    owner=MockUsersMongoSheldon(),
                    )
                )
            post = Post(**popt)
        except PostError as e:
            print(e)
        else:
            self.assertTrue(post.valid())
            self.assertTrue(post.allowed())

    def test_by(self):
        try:
            popt = dict(
                pid="5298dbe156c153264f3ea920",
                # c_user=MockUsersMongoZamony(),
                mongo=posts_mongo,
                # post=dict(
                #     content="Пост",
                #     owner=MockUsersMongoSheldon(),
                #     )
                )
            post = Post(**popt)
        except PostError as e:
            print(e)
        else:
            self.assertTrue(post.by("zamony"))
            self.assertEqual(type(post.by()), str)

    def test_delete(self):
        try:
            popt = dict(
                pid="5298dbe156c153264f3ea920",
                c_user=MockUsersMongoZamony(),
                mongo=posts_mongo,
                # post=dict(
                #     content="Пост",
                #     owner=MockUsersMongoSheldon(),
                #     )
                )
            post = Post(**popt)
        except PostError as e:
            print(e)
        else:
            self.assertEqual(post.delete(), None)

    def test_like(self):
        try:
            popt = dict(
                pid="5298dbe156c153264f3ea920",
                c_user=MockUsersMongoZamony(),
                mongo=posts_mongo,
                # post=dict(
                #     content="Пост",
                #     owner=MockUsersMongoSheldon(),
                #     )
                )
            post = Post(**popt)
        except PostError as e:
            print(e)
        else:
            self.assertEqual(post.like(), -1)

    def test_reblog(self):
        try:
            popt = dict(
                pid="5298dbe156c153264f3ea920",
                c_user=MockUsersMongoSheldon(),
                mongo=posts_mongo,
                # post=dict(
                #     content="Пост",
                #     owner=MockUsersMongoSheldon(),
                #     )
                )
            post = Post(**popt)
        except PostError as e:
            print(e)
        else:
            self.assertIsNotNone(post.reblog())

    # def test_comment(self):
    #     self.assertTrue(False)

    # def test_like(self):
    #     self.assertTrue(False)

    # def test_reblog(self):
    #     self.assertTrue(False)

class TestComment(unittest.TestCase):
    def test_current(self):
        copt = dict(
            # pid="5298dbe156c153264f3ea920",
            c_user=MockUsersMongoZamony(),
            mongo=comments_mongo,
            content="SSSSAS"
            )
        com = Comment(**copt)
        self.assertIsNotNone(com)


if __name__ == "__main__":
    unittest.main()