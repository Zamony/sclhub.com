import smtplib
import json
import uuid
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class RegisterManager(object):

    def __init__(self, redis, mongo):
        self.redis = redis
        self.mongo = mongo

    def confirm(code):
        t_user=self.redis.get(code)
        if t_user is not None:
            self.add_user(t_user)
            return True
        return False

    def send_confirmation(profile):
        msg = email.mime.multipart.MIMEMultipart()
        link = "://sclhub.com/confirm?code=" + profile["code"]
        msg['From'] = 'SclHub.com'
        msg['To'] = profile["email"]
        msg['Subject'] = 'Подтверждение регистрации на SclHub.com'
        message = 'Учетная запись с именем {0}(@{1}) создана. Пройдите по следующей'\
        'ссылке, чтобы стать полноправным участником сети:'\
        '<br><a href="{2}">{2}</a>'.format(profile["full_name"]profile["username"], link)
        msg.attach(email.mime.text.MIMEText(message))
        mailserver = smtplib.SMTP('localhost')
        mailserver.sendmail('SclHub.com', profile["email"], msg.as_string())
        mailserver.quit()

    def add_user(profile):
        self.mongo.users.profiles.insert(profile)

    def create_temp_user(self, username, password, email):
        u_opt = dict(
            username=username,
            password=password,
            email=email
            )
        self.redis.setex(uuid.uuid4(), json.dumps(u_opt))