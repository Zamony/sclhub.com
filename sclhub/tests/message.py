import unittest
import time
from bson.objectid import ObjectId

c_user = {
        "_id" : ObjectId("525eca2256c15315999a19fd"),
        "email" : "zamonyan@gmail.com",
        "friends" : [ "sheldon" ],
        "full_name" : "Nikita Mochalov",
        "password" : "aafe06e46da6f8158e7a90926b1b7a84",
        "username" : "zamony"
        }
receivers = [
            {"_id" : ObjectId("52774eb656c15307088f201c"),
             "email" : "sheldon@sheldon.reg",
             "friends" : [ "zamony" ],
             "full_name" : "Sheldon Cooper",
             "password" : "e10adc3949ba59abbe56e057f20f883e",
             "username" : "sheldon" },
            ]
msg = {
    "receivers" : ["sheldon"],
    "content" : "Lorem Ipsum dolor",
}
class SenderError(Exception):pass
class MessageSender():
    def __init__(self, c_user, mongo):
        assert type(c_user) != str, "c_user - это объект, а не строка"
        self.mongo = mongo
        self.c_user = c_user

    def send_message(self, msg):
        """
        Отправляет сообщение пользователю. В случае удачи возвращает True,
        в случае ошибки - False. Это абстрактный слой над классом Dialog, нужен
        для валидации сообщения
        """
        try:
            self._validate(msg)
        except (TypeError, KeyError):
            raise SenderError("Неправильное тело сообщения")
            return False
        except AssertionError:
            raise SenderError("Нужно выбрать друзей")
            return False
        try:
            if set(self.c_user["friends"]).intersection(msg["receivers"]) != msg["receivers"]:
                raise SenderError("Вы не в списке друзей получателя")
        except Exception as e:
            raise SenderError(str(e))
            return False
        else:
            dialog = Dialog(msg["receivers"])
            if dialog.push(msg):
                return True
        return False

    def _validate(self, msg):
        """
        Проверяет тело сообщения на ошибки. Порождает исключения в случае ошибок
        """
        msg["receivers"] = set(msg["receivers"])
        msg["content"] = str(msg["content"])
        assert len(msg["receivers"]) > 0, "Нужно выбрать друзей"
        assert msg["content"], "Пустое сообщение нельзя отправить"

class Dialog(object):
    id  = None

    def __init__(self, members, mongo):
        assert type(members) == list, "members must be list"
        self.mongo = mongo
        self.members = members
        if not self._exist():
            self._add()

    def _exist(self):
        """
        Проверяет сущестование диалога. True - если существует, False - если нет
        """
        dialog = self.mongo.users.dialogs.find_one({"members":{"$all":self.members}})
        if dialog is not None:
            self.id = dialog["id"]
            return True
        return False

    def _add(self):
        """
        Добавляет диалог. Ничего не возвращает. Устанавливает id
        """
        self.id = str(self.mongo.users.dialogs.insert({"members":self.members}))

    def push(self, msg):
        """
        Отправляет сообщение в диалог. True - удачно, False - нет
        """
        msg_id = self.mongo.users.messages.insert(dict(
                dialog_id=self.id,
                msg_author=self.c_user["username"],
                msg_content=msg["content"],
                date=time.strftime("%d.%m.%y - %H:%M", time.localtime())
                ))
        if msg is not None:
            self.mongo.users.dialogs.update({"_id":ObjectId(self.id)},
                                            {"$set":{"last":ObjectId(msg_id)}})
            return True
        else:
            return False




class TestMessageHandler(unittest.TestCase):
    def test_send_message(self):
        sender = MessageHandler();
        self.assertTrue(sender.send_message())
        # msg["receivers"] = ["sheldon", "cooper"]
        # self.assertTrue(sender.send_message())
        # msg["receivers"] = []
        # self.assertTrue(sender.send_message())
        # msg["msg_content"] = ""
        # self.assertTrue(sender.send_message())

class TestDialog(unittest.TestCase):
    def test_exist(self):
        self.assertTrue(self._exist())

if __name__ == "__main__":
    unittest.main();