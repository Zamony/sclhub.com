from bson.objectid import ObjectId
import json
import tornado.web
import sclhub.redis_request_handler
import sclhub.tools.user


class SearchHandler(sclhub.redis_request_handler.RedisRequestHandler):
    redis = None
    loader = None
    mongo = None

    def initialize(self, redis, loader, mongo):
        self.redis = redis
        self.loader = loader
        self.mongo = mongo

    def get(self):
        user = sclhub.tools.user.User(
            self.current_user, self.redis, self.mongo)

        online_friends = user.online_friends
        online = []
        for onl in online_friends:
            onl = onl.decode("utf-8")
            if onl != self.current_user:
                online_user = sclhub.tools.user.User(onl, None, self.mongo)
                online.append(online_user.profile)

        html = self.loader.load("search.html").generate(
            profile=user.profile,
            current_user=self.current_user,
            online=online
        )
        self.write(html)

    def post(self, search):
        try:
            offset = self.get_argument("offset")
        except tornado.web.MissingArgumentError:
            offset = 0
        if search[0] == "@":
            users = self.username_search(search[1:], offset)
        else:
            search = search.split(" ", 1)
            if len(search) == 2:
                users = self.name_search(search, offset)
        response = dict()
        for user in users:
            response[str(user["username"])] = dict(
                full_name=user["full_name"],
                pic=user["pic"],
                avatar=user["avatar"],
                )
        if users is None:
            self.write("{}")
        else:
            self.write(json.dumps(response))


    def username_search(self, username, offset):
        return self.mongo.users.profiles.find({"username": {"$regex":
            "[a-z_]*"+username+"[a-z_]*", "$options":"i"}}).skip(offset).limit(15)

    def name_search(self, search, offset):
        pattern = "({0}\ {1})|({1}\ {0})".format(search[0], search[1])
        return self.mongo.users.profiles.find({"full_name": {"$regex":pattern}}).skip(offset).limit(15)
