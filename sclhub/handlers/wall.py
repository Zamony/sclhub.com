import time, json
from bson.objectid import ObjectId
import tornado.web
import tornado.escape
import sclhub.redis_request_handler

class BlogHandler(sclhub.redis_request_handler.RedisRequestHandler):
	mongo = None
	redis = None

	def initialize(self, mongo, redis):
		self.mongo = mongo
		self.redis = redis

	@tornado.web.authenticated
	def post(self, wall_owner):
		try:
			post_content = tornado.escape.xhtml_escape(self.get_argument("content"))
			if not post_content:
				raise Exception("Пустое сообщение нельзя добавить")
			post_date = time.strftime("%d.%m.%y - %H:%M", time.localtime())
			post_id = self.mongo.users.walls.insert(dict(
				wall_owner=wall_owner,
				post_content=post_content,
				post_date=post_date,
				post_author=self.current_user,
				post_likes=[],
				post_reblog=[]
				))
		except Exception as e:
			self.set_status(400)
			self.write(json.dumps(dict(
				error=str(e)
			)))
		else:
			response = json.dumps(dict(
				post_author=self.current_user,
				post_id=str(post_id),
				post_date=post_date,
				post_content=post_content,
				type="add_post"
				))
			self.redis.publish("wall.{}".format(wall_owner), response)
			self.set_status(201)
			# self.write(response)
	@tornado.web.authenticated
	def delete(self, post_id):
		post = self.mongo.users.walls.find_one({"_id":ObjectId(post_id), "wall_owner":self.current_user})
		if post is None:
			post = self.mongo.users.walls.find_one({"_id":ObjectId(post_id), "post_author":self.current_user})
		if post is not None:
			self.mongo.users.walls.remove({"_id":ObjectId(post_id)})
			self.mongo.users.comments.remove({"post_id":post_id})
			response = json.dumps(dict(
				type="delete_post",
				post_id=post_id
				))
			self.redis.publish("wall.{}".format(self.get_argument("wall_owner")), response)
			self.set_status(202)
			self.write(json.dumps(dict(
				error=0,
			)))
		else:
			self.set_status(400)
			self.write(json.dumps(dict(
				error="Ошибка удаления",
			)))

class CommentHandler(sclhub.redis_request_handler.RedisRequestHandler):
	mongo = None
	redis = None

	def initialize(self, mongo, redis):
		self.mongo = mongo
		self.redis = redis

	@tornado.web.authenticated
	def post(self, post_id):
		try:
			comment_content = tornado.escape.xhtml_escape(self.get_argument("content"))
			if not comment_content:
				raise Exception("Пустые комментарии недопустимы")
			if self.mongo.users.walls.find_one({"_id":ObjectId(post_id)}) is None:
				raise Exception("Вы пытаетесь добавить комментарий к несуществующей записи")
			comment_date = time.strftime("%d.%m.%y - %H:%S", time.localtime())
			comment_id = self.mongo.users.comments.insert(dict(
				post_id=post_id,
				comment_content=comment_content,
				comment_date=comment_date,
				comment_author=self.current_user,
				comment_likes=[]
				))
		except Exception as e:
			self.set_status(400)
			self.write(json.dumps(dict(
				error=str(e),
			)))
		else:
			response = json.dumps(dict(
				comment_author=self.current_user,
				comment_id=str(comment_id),
				comment_date=comment_date,
				comment_content=comment_content,
				post_id=post_id,
				type="add_comment"
				))
			self.redis.publish("wall.{}".format(self.get_argument("wall_owner")), response)
			self.set_status(201)
			# self.write(response)
	@tornado.web.authenticated
	def delete(self, comment_id):
		comment = self.mongo.users.comments.find_one({"_id":ObjectId(comment_id), "comment_author":self.current_user})
		if comment is None:
			comment = self.mongo.users.comments.find_one({"_id":ObjectId(comment_id)})
			if self.mongo.users.walls.find_one({"wall_owner":self.current_user, "_id": ObjectId(comment["post_id"])}) is None:
				comment = None
		if comment is not None:
			self.mongo.users.comments.remove({"_id":ObjectId(comment_id)})
			self.set_status(202)
			response = json.dumps(dict(
				comment_id=comment_id,
				type="delete_comment"
				))
			self.redis.publish("wall.{}".format(self.get_argument("wall_owner")), response)
			# self.write(response)
		else:
			self.set_status(400)
			self.write(json.dumps(dict(
				error="Ошибка удаления. Комментарий не принадлежит этому пользователю",
			)))

class LikeHandler(sclhub.redis_request_handler.RedisRequestHandler):
	mongo = None
	redis = None

	def initialize(self, mongo, redis):
		self.mongo = mongo
		self.redis = redis

	@tornado.web.authenticated
	def post(self, post_id):
		try:
			if self.mongo.users.walls.find_one({"_id":ObjectId(post_id)}) is None:
				raise Exception("Вы пытаетесь добавить лайк к несуществующей записи")
			if self.mongo.users.walls.find_one({"_id":ObjectId(post_id), "post_likes": self.current_user}) is not None:
				raise Exception("Вы уже поставили лайк на эту запись")
			self.mongo.users.walls.update({"_id": ObjectId(post_id)}, {"$push": {"post_likes": self.current_user}})
		except Exception as e:
			self.set_status(400)
			self.write(json.dumps(dict(
				error=str(e),
			)))
		else:
			response = json.dumps(dict(
					action=1,
					type="like",
					post_id=post_id
				))
			self.redis.publish("wall.{}".format(self.get_argument("wall_owner")), response)
			self.set_status(201)
			# self.write(response)
	@tornado.web.authenticated
	def delete(self, post_id):
		try:
			if self.mongo.users.walls.find_one({"_id":ObjectId(post_id)}) is None:
				raise Exception("Вы пытаетесь добавить лайк к несуществующей записи")
			self.mongo.users.walls.update({"_id": ObjectId(post_id)}, {"$pull": {"post_likes": self.current_user}})
		except Exception as e:
			self.set_status(400)
			self.write(json.dumps(dict(
				error=str(e),
			)))
		else:
			response = json.dumps(dict(
					action=-1,
					type="like",
					post_id=post_id
				))
			self.redis.publish("wall.{}".format(self.get_argument("wall_owner")), response)
			self.set_status(201)
			# self.write(response)

class ReblogHandler(sclhub.redis_request_handler.RedisRequestHandler):
	mongo = None
	redis = None

	def initialize(self, mongo, redis):
		self.mongo = mongo
		self.redis = redis

	@tornado.web.authenticated
	def post(self, post_id):
		try:
			if self.mongo.users.walls.find_one({"_id":ObjectId(post_id)}) is None:
				raise Exception("Вы пытаетесь сделать реблог несуществующей записи")
			if self.mongo.users.walls.find_one({"_id":ObjectId(post_id), "post_reblog": self.current_user}) is not None:
				raise Exception("Вы уже сделали реблог на эту запись")
			if self.mongo.users.walls.find_one({"_id":ObjectId(post_id), "post_author": self.current_user}):
				raise Exception("Нельзя сделать реблог собственной записи")
			if self.mongo.users.walls.find_one({"_id":ObjectId(post_id), "wall_owner": self.current_user}):
				raise Exception("Нельзя сделать реблог записи на своей стене")
			self.mongo.users.walls.update({"_id": ObjectId(post_id)}, {"$push": {"post_reblog": self.current_user}})
			blog_post = self.mongo.users.walls.find_one({"_id": ObjectId(post_id)})
			blog_post["wall_owner"] = self.current_user
			blog_post["post_likes"] = []
			try:
				blog_post["origin_id"]
			except KeyError:
				blog_post["origin_id"] = blog_post["_id"]
			del blog_post["_id"]
			blog_post_id = self.mongo.users.walls.insert(blog_post)
			blog_post = self.mongo.users.walls.find_one({"_id": blog_post_id })
		except Exception as e:
			self.set_status(400)
			self.write(json.dumps(dict(
				error=str(e),
			)))
		else:
			response = dict(
					action=1,
					type="reblog",
					post_id=str(post_id)
				)
			self.redis.publish("wall.{}".format(self.get_argument("wall_owner")), json.dumps(response))
			response = json.dumps(dict(
				post_author=blog_post["post_author"],
				post_id=str(blog_post["_id"]),
				post_date=blog_post["post_date"],
				post_content=blog_post["post_content"],
				type="add_post"
				))
			# blog_post["_id"], blog_post["origin_id"] = str(blog_post["_id"]), str(blog_post["origin_id"])
			# response = json.dumps(blog_post)
			self.redis.publish("wall.{}".format(self.current_user), response)
			self.set_status(201)
			# self.write("")

	@tornado.web.authenticated
	def delete(self, post_id):
		try:
			post = self.mongo.users.walls.find_one({"_id":ObjectId(post_id)})
			if post is None:
				raise Exception("Вы пытаетесь убрать реблог к несуществующей записи")
			if post["wall_owner"] == post["post_author"]:
				self.mongo.users.walls.update({"_id": ObjectId(post["_id"])}, {"$pull": {"post_reblog": self.current_user}})
			else:
				self.mongo.users.walls.update({"_id": ObjectId(post["origin_id"])}, {"$pull": {"post_reblog": self.current_user}})
			#TODO: этот запрос удаляет ретвит с записи 
			try:
				self.mongo.users.walls.remove({"wall_owner":self.current_user, "origin_id":ObjectId(post["origin_id"])})
			except KeyError:
				self.mongo.users.walls.remove({"wall_owner":self.current_user, "origin_id":ObjectId(post["_id"])})
		except Exception as e:
			self.set_status(400)
			self.write(json.dumps(dict(
				error=str(e),
			)))
		else:
			response = json.dumps(dict(
					action=-1,
					type="reblog",
					post_id=post_id
				))
			self.redis.publish("wall.{}".format(self.get_argument("wall_owner")), response)
			blog_post = dict(
				type="delete_post",
				post_id=post_id
				)
			response = json.dumps(blog_post)
			self.redis.publish("wall.{}".format(self.current_user), response)
			self.set_status(201)
			self.write(json.dumps(dict(
					error=0
				)))

class FriendHandler(sclhub.redis_request_handler.RedisRequestHandler):
	mongo = None
	redis = None

	def initialize(self, mongo, redis):
		self.mongo = mongo
		self.redis = redis

	@tornado.web.authenticated
	def post(self, username):
		current_user_profile = self.mongo.users.profiles.find_one({"username":self.current_user})
		try:
			if username in current_user_profile["friends"]:
				raise Exception("Этот пользователь уже у вас в друзьях")
			if username == self.current_user:
				raise Exception("Вы не можете добавить себя в друзья")
			if (self.mongo.users.notifications.find_one({"username":username, "friend_username": self.current_user}) is not None
			or self.mongo.users.notifications.find_one({"username":self.current_user, "friend_username": username}) is not None):
				raise Exception("Заявка уже отправлена")
		except Exception as e:
			self.set_status(400)
			self.write(str(e))
		else:
			self.mongo.users.notifications.insert(dict(
				username=username,
				type="friendship",
				friend_username=self.current_user,
				date=time.strftime("%d.%m.%y - %H:%M", time.localtime()
				)))
			self.write("")

	@tornado.web.authenticated
	def put(self, username):
		self.mongo.users.notifications.remove(dict(
			type="friendship",
			friend_username=self.current_user,
			username=username
			))
		self.mongo.users.notifications.remove(dict(
			type="friendship",
			friend_username=username,
			username=self.current_user
			))
		self.write("")

	@tornado.web.authenticated
	def delete(self, username):
		c_user =  self.current_user
		self.mongo.users.profiles.update({"username": username}, {"$pull": {"friends": c_user}})
		self.mongo.users.profiles.update({"username": c_user}, {"$pull": {"friends": username}})
		self.write("")

class MessageHandler(sclhub.redis_request_handler.RedisRequestHandler):
	mongo = None
	redis = None

	def initialize(self, mongo, redis):
		self.mongo = mongo
		self.redis = redis

	@tornado.web.authenticated
	def post(self, username):
		c_user = self.current_user
		profile = self.mongo.users.profiles.find_one({"username":self.current_user})
		dialog = self.mongo.users.dialogs.find_one({"buddies":[username, c_user]})
		if dialog is not None:
			msg_content = tornado.escape.xhtml_escape(self.get_argument("content"))
			self.messages.insert(dict(
				msg_author=c_user,
				msg_content=msg_content,
				date=time.strftime("%d.%m.%y - %H:%M", time.localtime())
				))
			#Сделать добавление данных о последнем сообщении
		elif username in profile["friends"]:
			self.mongo.users.dialogs.insert(dict(
				dialog_members=[c_user, username]
				#Сделать добавление данных о новом сообщении
				))
