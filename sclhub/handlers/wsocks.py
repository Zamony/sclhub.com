import json
import redis
import time
from bson.objectid import ObjectId
import tornadoredis, tornadoredis.pubsub
from sockjs.tornado import SockJSRouter, SockJSConnection
from pymongo import MongoClient
import sclhub.tools.messages
import sclhub.tools.blog
import sclhub.tools.user

subscriber = tornadoredis.pubsub.SockJSSubscriber(tornadoredis.Client())
redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)
trusted_users = {}

class WSocksHandler(SockJSConnection):
    def on_open(self, session):
        pass
        # session_key = session.cookies["session_data"].value
        # self.subscribers[session_key] = self.redis.pubsub()

    def on_message(self, msg):
        print(msg)
        msg = json.loads(msg)
        try:
            if msg["type"] == "subscribition":
                # Предотвращение подписки на сообщения
                if not msg["channel_type"].startswith("msg"):
                    subscriber.subscribe(["{}.{}".format(msg["channel_type"], msg["channel_id"])], self)
            elif msg["type"] == "add_post":
                self.user = redis_client.get("session." + msg["skey"]).decode("utf-8")
                self.mongo = MongoClient()
                c_user = sclhub.tools.user.User(self.user, redis_client, self.mongo)
                popt = dict(
                    mongo=self.mongo,
                    post={"content":msg["content"], "owner":sclhub.tools.user.User(msg["owner"], redis_client, self.mongo)},
                    c_user=c_user
                    )
                post = sclhub.tools.blog.Post(**popt)
                post.current["_id"] = str(post.current["_id"])
                post.current["type"] = "add_post"
                post.current["pic"] = c_user.profile["pic"]
                redis_client.publish("wall.{}".format(post.current["owner"]), json.dumps(post.current))
            elif msg["type"] == "del_post":
                self.user = redis_client.get("session." + msg["skey"]).decode("utf-8")
                self.mongo = MongoClient()
                c_user = sclhub.tools.user.User(self.user, redis_client, self.mongo)
                popt = dict(
                    mongo=self.mongo,
                    c_user=c_user,
                    pid=msg["pid"]
                    )
                post=sclhub.tools.blog.Post(**popt)
                post.delete()
                response = dict(
                    type="delete_post",
                    pid=msg["pid"]
                    )
                redis_client.publish("wall.{}".format(post.current["owner"]), json.dumps(response))
            elif msg["type"] == "add_comment":
                self.user = redis_client.get("session." + msg["skey"]).decode("utf-8")
                self.mongo = MongoClient()
                c_user = sclhub.tools.user.User(self.user, redis_client, self.mongo)
                popt = dict(
                    mongo=self.mongo,
                    pid=msg["pid"],
                    c_user=c_user
                    )
                post = sclhub.tools.blog.Post(**popt)
                post._post["owner"] = sclhub.tools.user.User(post.current["owner"], redis_client, self.mongo)
                com = post.comment(msg["content"])
                com["type"] = "add_comment"
                com["pic"] = c_user.profile["pic"]
                com["_id"] = str(com["_id"])
                com["pid"] = msg["pid"]
                redis_client.publish("wall.{}".format(com["author"]), json.dumps(com))
            elif msg["type"] == "del_comment":
                self.user = redis_client.get("session." + msg["skey"]).decode("utf-8")
                self.mongo = MongoClient()
                c_user = sclhub.tools.user.User(self.user, redis_client, self.mongo)
                owner = sclhub.tools.user.User(self.user, redis_client, self.mongo)
                popt = dict(
                    mongo=self.mongo,
                    pid=msg["pid"],
                    c_user=c_user,
                    )
                post = sclhub.tools.blog.Post(**popt)
                com = post.uncomment(msg["cid"])
                redis_client.publish("wall.{}".format(post.current["owner"]), json.dumps({"type":"delete_comment", "cid":msg["cid"]}))
            elif msg["type"] == "like":
                self.user = redis_client.get("session." + msg["skey"]).decode("utf-8")
                self.mongo = MongoClient()
                c_user = sclhub.tools.user.User(self.user, redis_client, self.mongo)
                owner = sclhub.tools.user.User(self.user, redis_client, self.mongo)
                popt = dict(
                    mongo=self.mongo,
                    pid=msg["pid"],
                    c_user=c_user,
                    )
                post = sclhub.tools.blog.Post(**popt)
                act = post.like()
                redis_client.publish("wall.{}".format(post.current["owner"]), json.dumps({"type":"like", "pid":msg["pid"],"act":act}))
            elif msg["type"] == "reblog":
                self.user = redis_client.get("session." + msg["skey"]).decode("utf-8")
                self.mongo = MongoClient()
                c_user = sclhub.tools.user.User(self.user, redis_client, self.mongo)
                owner = sclhub.tools.user.User(self.user, redis_client, self.mongo)
                popt = dict(
                    mongo=self.mongo,
                    pid=msg["pid"],
                    c_user=c_user,
                    )
                post = sclhub.tools.blog.Post(**popt)
                pid = post.reblog()
                act = 1 if pid is not None else -1
                redis_client.publish("wall.{}".format(post.current["owner"]), json.dumps({"type":"reblog", "pid":msg["pid"],"act": act}))
            elif msg["type"] == "load_posts":
                if msg["page"] == "personal":
                    self.mongo = MongoClient()
                    posts_cursor = self.mongo.users.walls.find({"owner":msg["owner"]}).limit(7).sort([("date", -1), ("_id", -1)]).skip(msg["count"])
                    posts = []
                    for post in posts_cursor:
                        post["_id"] = str(post["_id"])
                        author = sclhub.tools.user.User(post["author"], redis_client, self.mongo)
                        post["pic"] = author.profile["pic"]
                        try:
                            post["origin"] = str(post["origin"])
                        except KeyError:
                            pass
                        posts.append(post)
                    redis_client.publish("wall.{}".format(msg["owner"]), json.dumps(dict(posts=posts, type="load_posts")))
            elif msg["type"] == "load_coms":
                self.mongo = MongoClient()
                popt = dict(
                    pid=msg["pid"],
                    mongo=self.mongo,
                    redis=redis_client
                    )
                post = sclhub.tools.blog.Post(**popt)
                coms = post.load_comments(start=int(msg["count"]), lim=0)
                redis_client.publish("wall.{}".format(post.current["owner"]),
                    json.dumps({"type":"load_coms", "comments":coms}))
        except Exception as e:
            self.send('{"type":"error", "error":"' + str(e) + '"}')

    def on_close(self):
        pass


class MessagesHandler(SockJSConnection):
    def on_open(self, session):
        pass

    def on_message(self, msg):
        try:
            msg = json.loads(msg)
            self.user = redis_client.get("session." + msg["skey"]).decode("utf-8")
            self.mongo = MongoClient()
            user_generator = sclhub.tools.user.SimpleUser(redis_client, self.mongo)
            if msg["type"] == "subscribe_all":
                dialogs = self.mongo.users.dialogs.find({"members":{"$in":[self.user]}})
                channels = []
                for dialog in dialogs:
                    channels.append("msg.{}".format(str(dialog["_id"])))
                subscriber.subscribe(channels, self)
            elif msg["type"] == "subscribe_one":
                if self.is_dialog_owner(msg["dialog_id"]):
                    subscriber.subscribe(["msg.{}".format(msg["dialog_id"])], self)
            elif msg["type"] == "push":
                if self.is_dialog_owner(msg["dialog_id"]):
                    c_user = self.mongo.users.profiles.find_one({"username": self.user})
                    sender = sclhub.tools.messages.MessageSender(c_user, self.mongo)
                    msg["receivers"] = self.mongo.users.dialogs.find_one({"_id":ObjectId(msg["dialog_id"])})["members"]
                    msg["receivers"].remove(c_user["username"])
                    data = sender.send_message(msg)
                    data["_id"] = str(data["_id"])
                    data["type"] = "msg"
                    author = sclhub.tools.user.User(data["msg_author"], None, self.mongo).profile
                    data["full_name"] = author["full_name"]
                    data["pic"] = author["pic"]
                    redis_client.publish("msg.{}".format(msg["dialog_id"]), json.dumps(data))
                    dialog_update = dict(
                        dialog_id=msg["dialog_id"],
                        msg_content=data["msg_content"],
                        type="dialog_update"
                        )
                    redis_client.publish("msg.{}".format(msg["dialog_id"]), json.dumps(dialog_update))
            elif msg["type"] == "more":
                if self.is_dialog_owner(msg["dialog_id"]):
                    msgs_cursor = self.mongo.users.messages.find({"dialog_id":msg["dialog_id"]}).sort("date", -1)
                    skipped = msgs_cursor.count() - int(msg["count"])
                    skipped = skipped if skipped > 0 else 0
                    # Константа, ограничивающая число сообщений
                    limit = 30
                    msgs_cursor.skip(skipped).limit(limit)
                    messages = []
                    for full_msg in msgs_cursor:
                        message = {}
                        message["msg_author"] = full_msg["msg_author"]
                        message["msg_content"] = full_msg["msg_content"]
                        message["author"] = user_generator.get(full_msg["msg_author"])
                        user_generator.book()
                        messages.append(message)
                    response = dict(
                        messages=messages,
                        type="more",
                        more=(1 if skipped - (30 + limit) > 0 else 0)
                        )
                    redis_client.publish("msg.{}".format(msg["dialog_id"]), json.dumps(response))
        except Exception as e:
            self.send('{"type":"error","error":"' + str(e) + '"}')
            #print('{"error":"Неправильное тело запроса:'+ str(e) +'"}')

    def is_dialog_owner(self, dialog_id):
        if (self.mongo.users.dialogs.find_one({"_id":ObjectId(dialog_id),
                                        "members":{"$in":[self.user]}}) is not None):
            return True
        return False

    def on_close(self):
        pass