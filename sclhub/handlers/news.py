from bson.objectid import ObjectId
import sclhub.redis_request_handler
import sclhub.tools.user


class NewsHandler(sclhub.redis_request_handler.RedisRequestHandler):
    redis = None
    loader = None
    mongo = None

    def initialize(self, redis, loader, mongo):
        self.redis = redis
        self.loader = loader
        self.mongo = mongo

    def get(self):
        user = sclhub.tools.user.User(self.current_user, self.redis, self.mongo)
        posts = sclhub.tools.blog.Tape(user, self.redis, self.mongo)
        posts.load([self.current_user] + user.profile["friends"])
        print(user.profile["friends"] + [self.current_user])
        posts.init_comments(lim=4)
        posts_count = posts.count()

        online_friends = user.online_friends
        online = []
        for onl in online_friends:
            onl = onl.decode("utf-8")
            if onl != self.current_user:
                online_user = sclhub.tools.user.User(onl, None, self.mongo)
                online.append(online_user.profile)

        html = self.loader.load("news.html").generate(
                posts=posts.get(), comment_label_format=self.comment_label_format,
                current_user=self.current_user,
                profile=user.profile, online=online, more=(True if posts.count() > posts.limit else False)
            )
        self.write(html)

    def comment_label_format(self, count):
        if count == 0:
            return "Добавить ответ"
        elif count%10 == 1:
            return "Показать 1 ответ"
        elif count%10 >=2 and count%10 <=4:
            return "Показать {} ответа".format(count)
        else:
            return "Показать {} ответов".format(count)