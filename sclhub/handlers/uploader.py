import json
import tornado.web
import sclhub.redis_request_handler
import sclhub.tools.uploader
import sclhub.tools.user

class ImageUploaderHandler(sclhub.redis_request_handler.RedisRequestHandler):
    redis = None
    mongo = None

    def initialize(self, redis, mongo):
        self.redis = redis
        self.mongo = mongo

    @tornado.web.authenticated
    def put(self, *type):
        # Определяем тип обработчика - первое не None значение
        type = [i for i in type if i != None][0]
        if type == "temp":
            self.temp_handler()
        elif type == "avatar":
            self.avatar_handler()
        elif type == "perm":
            self.perm_handler()
        else:
            self.set_status(400)

    def temp_handler(self):
        blob = bytes(self.request.files["files[]"][0]["body"])
        image = sclhub.tools.uploader.ImageUploader(blob)
        image.resize(500)
        response = json.dumps(dict(url=image.save()))
        self.write(response)

    def avatar_handler(self):
        try:
            url = self.get_argument("url")
            info = {
                "l":int(self.get_argument("l")),
                "t":int(self.get_argument("t")),
                "w":int(self.get_argument("w")),
                "h":int(self.get_argument("h")),
            }
        except tornado.web.MissingArgumentException:
            self.set_status(400)
            return None
        else:
            url = url.split("/").pop()
            image = sclhub.tools.uploader.ImageUploader()
            image.load(url)
            image.crop(info)
            cropped = sclhub.tools.uploader.ImageUploader()
            user = sclhub.tools.user.User(self.current_user, self.redis, self.mongo)
            updates = dict(
                pic=image.save(True),
                avatar=cropped.save(True, url),
                )
            user.update_profile(updates, False)
            self.write(json.dumps(updates))

    def perm_handler(self):
        pass
