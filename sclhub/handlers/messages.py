import json
from bson.objectid import ObjectId
import tornado.web
import sclhub.redis_request_handler
import sclhub.tools.messages
import sclhub.tools.user

class MessagesHandler(sclhub.redis_request_handler.RedisRequestHandler):
    loader = None
    mongo = None

    def initialize(self, loader, mongo, redis):
        self.redis = redis
        self.loader = loader
        self.mongo = mongo

    @tornado.web.authenticated
    def get(self):
        try:
            dialog_id = self.get_argument("did")
            if (self.mongo.users.dialogs.find_one({"_id": ObjectId(dialog_id),
                                                   "members": {"$in": [self.current_user]}}) is None):
                raise Exception("Dialog not found")
        except Exception:
            self.redirect("/400")
        else:
            dialogs = self.mongo.users.dialogs.find(
                {"_id": {"$ne": ObjectId(dialog_id)},
                 "members": {"$in": [self.current_user]}}).limit(7)
            dialog = self.mongo.users.dialogs.find_one(
                {"_id": ObjectId(dialog_id)})
            dialog["members"].remove(self.current_user)
            messages = self.mongo.users.messages.find({"dialog_id": dialog_id})
            user = sclhub.tools.user.User(self.current_user, self.redis, self.mongo)
            html = self.loader.load(
                "messages.html").generate(receivers=dialog["members"],
                                          dialog_id=dialog_id,
                                          messages=messages,
                                          current_user=self.current_user,
                                          dialogs=dialogs,
                                          profile=user.profile)
            self.write(html)




class DialogsHandler(sclhub.redis_request_handler.RedisRequestHandler):
    loader = None
    mongo = None

    def initialize(self, loader, mongo, redis):
        self.redis = redis
        self.loader = loader
        self.mongo = mongo

    @tornado.web.authenticated
    def get(self):
        user = sclhub.tools.user.User(self.current_user, self.redis, self.mongo)
        c_user = user.profile
        modal = {"receiver": None, "show": 0}
        try:
            modal["receiver"] = self.get_argument("receiver")
        except tornado.web.MissingArgumentError:
            pass

        dialogs_cursor = self.mongo.users.dialogs.find(
            {"members": {"$in": [self.current_user]}}).sort("date", -1)
        dialogs = []
        count = 0
        user_generator = sclhub.tools.user.SimpleUser(self.redis, self.mongo)
        for dialog in dialogs_cursor:
            msg = {}
            dialog["messages"] = []
            if count == 0:
                # Только 30 сообщений подгружаются вначале
                skipped = self.mongo.users.messages.count() - 15
                msgs = self.mongo.users.messages.find({"dialog_id": 
                    str(dialog["_id"])}).skip(skipped if skipped > 0 else 0)
                for msg in msgs:
                    msg["author"] = user_generator.get(msg["msg_author"])
                    dialog["messages"].append(msg)
            if msg != {}:
                buddy = user_generator.get(msg["msg_author"])
                dialog["full_name"] = buddy["full_name"]
                dialog["last_msg"] = self.mongo.users.messages.find_one({"_id":dialog["last"]})
                dialogs.append(dialog)
                count += 1

        # print(dialogs[0])
        template_options = dict(
            friends=c_user["friends"],
            modal=modal,
            dialogs=dialogs,
            current_user=self.current_user,
            loading_possibility = (True if skipped > 0 else False),
            c_user=c_user,
            current_dialog=(dialogs[0] if dialogs != [] else None)
        )
        html = self.loader.load("dialogs.html").generate(profile=c_user, **template_options)
        self.write(html)

    @tornado.web.authenticated
    def post(self, prefix):
        msg = {
            "content": self.get_argument("content"),
            "receivers": self.get_argument("receivers")
        }
        if type(msg["receivers"]) != list:
            msg["receivers"] = [msg["receivers"]]
        c_user = self.mongo.users.profiles.find_one(
            {"username": self.current_user})
        sender = sclhub.tools.messages.MessageSender(c_user, self.mongo)
        try:
            sender.send_message(msg)
        except sclhub.tools.messages.SenderError as e:
            self.set_status(400)
            self.write(json.dumps({"error": str(e)}))
        else:
            self.write("")

