import tornado.web
import sclhub.tools.formvalidator

class RegisterHandler(tornado.web.RequestHandler):
    loader = None
    mongo = None

    def initialize(self, loader, mongo):
        self.loader = loader
        self.mongo = mongo

    def get(self):
        html = self.loader.load("register.html").generate()
        self.write(html)

    def post(self):
        try:
            reg_data = (self.get_argument("email"),
                self.get_argument("password"), self.get_argument("full_name"), self.get_argument("username"))
            map(sclhub.tools.formvalidator.FormValidator.validate, reg_data)
        except tornado.web.MissingArgumentError:
            self.write("All fields are required!")
        except sclhub.tools.formvalidator.FormValidationError as e:
            self.write("Some of arguments are invalid!")
        else:
            email, password, full_name, username = reg_data
            passw = hashlib.md5(password.encode('utf-8'))
            users_data = self.mongo.profiles;
            users_messages = self.mongo.messages;
            users_posts = self.mongo.posts;
            users_data.insert(dict(
                    password=passw.hexdigest(),
                    email=email,
                    full_name=full_name
                ))
            self.write("Thank you for the registration!")