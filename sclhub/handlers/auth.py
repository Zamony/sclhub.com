import hashlib
import time
import json
import uuid
import tornado.web
import sclhub.tools.formvalidator
import sclhub.tools.user
import sclhub.tools.auth
import sclhub.redis_request_handler

class LoginHandler(tornado.web.RequestHandler):
    username = None
    # 60 * 60 * 24 * 10 = 10 дней
    session_expire = 864000
    loader = None
    mongo = None
    redis = None
    def initialize(self, loader, mongo, redis):
        self.loader = loader
        self.mongo = mongo
        self.redis = redis
    def post(self):
        try:
            email, password, token = (self.get_argument("email"),
                self.get_argument("password"), self.get_argument("token"))
        except tornado.web.MissingArgumentError:
            self.write("Все поля обязательны для заполнения")
        else:
            if self.redis.hget("tokens", self.request.remote_ip).decode("utf-8") == token:
                password = hashlib.md5(password.encode('utf-8'))
                user = self.mongo.users.profiles.find_one({"email":email, "password": password.hexdigest()})
                if user is None:
                    self.write(json.dumps({"error":"Неправильное имя пользователя или пароль", "type":"error"}))
                else:
                    self.username = user["username"]
                    self.start_session()
                    self.current_user = self.username
                    self.write(json.dumps({"type":"redirect"}))
            else:
                # А тем временем заносим IP в блэк-лист
                self.write(json.dumps({"type":"error", "error":"Что-то пошло не так"}))
    get = post
    def start_session(self):
        session_key = hashlib.md5()
        session_key.update("{}{}f71dbe52628a3f83a77ab494817525c6".format(time.time(),
                            self.username).encode("utf-8"))
        session_key = session_key.hexdigest()
        try:
            self.redis.setex("session." + session_key,  self.session_expire, self.username)
        except:
            print ("Ошибка записи в Redis")
        return self.set_cookie("session_data", session_key, expires_days=10)


class RegisterHandler(sclhub.redis_request_handler.RedisRequestHandler):
    redis = None
    mongo = None

    def initialize(self, redis, mongo):
        self.redis = redis
        self.mongo = mongo

    def post(self):
        try:
            reg_data = dict(
                email=self.get_argument("email"),
                password=self.get_argument("password"),
                username=self.get_argument("username"),
                full_name=self.get_argument("full_name")
            )
            map(sclhub.tools.formvalidator.FormValidator.validate, reg_data)
            if self.mongo.users.profiles.find_one({"email":reg_data["email"]}):
                raise sclhub.tools.formvalidator.EmailAlreadyInUseError()
            elif self.mongo.users.profiles.find_one({"username":reg_data["username"]}):
                raise sclhub.tools.formvalidator.UsernameAlreadyInUseError()
        except tornado.web.MissingArgumentError:
            self.write("Нужно заполнить все поля")
        except sclhub.tools.formvalidator.FormValidationError as e:
            self.write("Проверьте правильность написания Email и имени")
        except sclhub.tools.formvalidator.EmailAlreadyInUseError:
            self.write("Такой Email уже используется")
        except sclhub.tools.formvalidator.UsernameAlreadyInUseError:
            self.write("Такой псевдоним уже используется")
        else:
            passw = hashlib.md5(reg_data["password"].encode('utf-8'))
            data = self.mongo.users.profiles;
            messages = self.mongo.users.messages;
            posts = self.mongo.users.posts
            rm = sclhub.tools.auth.RegisterManager(self.redis, self.mongo)
            reg_data["code"] = rm.create_temp_user(reg_data["username"],
                passw.hexdigest(), reg_data["full_name"], reg_data["email"])
            if reg_data["code"] is not None:
                rm.send_confirmation(reg_data)

    def get(self):
        rm = sclhub.tools.auth.RegisterManager(self.redis, self.mongo)
        if rm.confirm(self.get_argument("code")):
            self.write("Подтверждено")
        else:
            self.write("Не подтверждено. Попробуйте в другой раз")

class PasswordError(Exception):
    pass

class PasswordManager(sclhub.redis_request_handler.RedisRequestHandler):
    redis = None
    mongo = None
    user = None

    def initialize(self, redis, mongo):
        self.redis = redis
        self.mongo = mongo

    def put(self):
        response  = dict()
        try:
            passw = self.get_argument("password")
            passwr = self.get_argument("repeat")
            passwo = self.get_argument("old")
        except tornado.web.MissingArgumentError:
            self.set_status(400)
            response["type"] = "error"
            response["error"] = "Не хватает аргументов для обработки данных"
        else:
            try:
                self.user = sclhub.tools.user.User(self.current_user, self.redis, self.mongo)
                self.password = (passw, passwr, passwo)
            except (PasswordError, sclhub.tools.user.UserError) as e:
                self.set_status(400)
                response["type"] = "error"
                response["error"] = str(e)
            else:
                response["type"] = "OK"
        self.write(json.dumps(response))

    @property
    def password(self):
        pass

    @password.setter
    def password(self, passes):
        old = hashlib.md5(passes[2].encode('utf-8'))
        old = old.hexdigest()
        if old == self.user.profile["password"]:
            if passes[0] == passes[1]:
                password = hashlib.md5(passes[0].encode('utf-8'))
                password = password.hexdigest()
                self.user.update_profile({"password":password}, False)
            else:
                raise PasswordError("Пароли не совпадают")
        else:
            raise PasswordError("Старый пароль введен неверно")

class UserManager(object):
    pass

class ProfileInfoManager(sclhub.redis_request_handler.RedisRequestHandler):
    redis = None
    mongo = None
    user = None

    def initialize(self, redis, mongo):
        self.redis = redis
        self.mongo = mongo

    def put(self):
        response  = dict()
        try:
            self.user = sclhub.tools.user.User(self.current_user, self.redis, self.mongo)
            self.user.update_profile(self.arguments)
        except (sclhub.tools.user.UserError) as e:
            self.set_status(400)
            response["type"] = "error"
            response["error"] = str(e)
        else:
            response["type"] = "OK"
        self.write(json.dumps(response))
