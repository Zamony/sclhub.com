from bson.objectid import ObjectId
import sclhub.redis_request_handler
import sclhub.tools.user
import sclhub.tools.blog

class ProfileHandler(sclhub.redis_request_handler.RedisRequestHandler):
	redis = None
	loader = None
	mongo = None

	def initialize(self, redis, loader, mongo):
		self.redis = redis
		self.loader = loader
		self.mongo = mongo
	def get(self, user):
		userp = sclhub.tools.user.User(user, self.redis, self.mongo)
		if userp.profile is not None:
			posts = sclhub.tools.blog.Tape(userp, self.redis, self.mongo)
			posts.load([userp.profile["username"]])
			posts.init_comments(lim=4)
			posts_count = posts.count()

			current_user = self.current_user

			friendship = 0
			if (self.mongo.users.notifications.find_one({"username":user, "friend_username": current_user}) is not None
			or self.mongo.users.notifications.find_one({"username":current_user, "friend_username": user}) is not None):
				friendship = 1
			elif current_user in userp.profile["friends"]:
				friendship = 2

			online_friends = userp.online_friends
			online = []
			for onl in online_friends:
				onl = onl.decode("utf-8")
				if onl != self.current_user:
					online_user = sclhub.tools.user.User(onl, None, self.mongo)
					online.append(online_user.profile)

			html = self.loader.load("index.html").generate(
					posts=posts.get(), comment_label_format=self.comment_label_format,
					current_user=self.current_user, friendship=friendship,
					profile=sclhub.tools.user.User(self.current_user, None, self.mongo).profile, online=online, more=(True if posts.count() > posts.limit else False)
				)

			self.write(html)
		else:
			self.set_status(400)
	def comment_label_format(self, count):
		if count == 0:
			return "Добавить ответ"
		elif count < 0:
			return "Скрыть ответы"
		elif count%10 == 1:
			return "Показать еще 1 ответ"
		elif count%10 >=2 and count%10 <=4:
			return "Показать еще {} ответа".format(count)
		else:
			return "Показать еще {} ответов".format(count)