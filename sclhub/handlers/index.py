import uuid
import tornado.web
import sclhub.redis_request_handler
import sclhub.tools.user

class IndexHandler(tornado.web.RequestHandler):
    redis = None
    loader = None
    mongo = None

    def initialize(self, loader, redis, mongo):
        self.redis = redis
        self.loader = loader
        self.mongo = mongo
    def get(self):
        username = self.redis.get("session." + str(self.get_cookie("session_data")))
        #print(username)
        try:
            logout = self.get_argument("logout")
        except tornado.web.MissingArgumentError:
            logout = 0
        if username is not None and not logout:
            self.redirect("/"+username.decode("utf-8"));
        else:
            self.redis.delete("session.{}".format(self.get_cookie("session_data")))
            self.clear_cookie("session_data")
            token = str(uuid.uuid4()).replace("-", "")
            self.redis.hset("tokens", self.request.remote_ip, token)
            html = self.loader.load("auth.html").generate(token=token)
            self.write(html)

class ProfileHandler(sclhub.redis_request_handler.RedisRequestHandler):
    redis = None
    loader = None
    mongo = None

    def initialize(self, loader, redis, mongo):
        self.redis = redis
        self.loader = loader
        self.mongo = mongo

    @tornado.web.authenticated
    def get(self, username):
        c_username = self.redis.get("session." + self.get_cookie("session_data"))
        c_user = sclhub.tools.user.User(c_username, self.redis, self.mongo)
        print(c_user)
        html = self.loader.load("profile.html").generate(
            profile=user.profile,
            current_user=c_user
            )
        self.write(html)

    def post(self, username):
        # print(self.request.files)
        self.write("{}")