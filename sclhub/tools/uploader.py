import tempfile
import wand.image


class ImageUploaderError(Exception):
    pass


class ImageUploader(object):
    perm_path = None
    temp_path = None
    blob = None
    img_format = "jpeg"

    def __init__(self, blob=None, perm_path="static/img/",
                 temp_path="static/temp/"):
        self.perm_path = perm_path
        self.temp_path = temp_path
        self.blob = blob

    def load(self, name, permanent=False):
        """
        Записывает в self.blob данные изображение из временной или постоянной папки
        """
        d_dir = self.perm_path if permanent else self.temp_path
        with wand.image.Image(filename=(d_dir + name)) as img:
            self.blob = img.make_blob(self.img_format)
            return True
        return False



    def save(self, permanent=False, name=None):
        """
        Сохраняет изображение в директории с уникальным именем. Возврает
        путь к созданному файлу в формате /static/temp/sd21qa2.jpeg
        Если permanent=False, сохраняется во временную директорию. Иначе - на 
        постоянное хранение.
        Изображение берется из параметра blob конструктора. Если permanent=True
        и name not None, то из временной директории
        """
        d_dir = self.perm_path if permanent else self.temp_path
        img_file = tempfile.NamedTemporaryFile(
            dir=d_dir, prefix="", suffix="." + self.img_format, delete=False)
        if permanent and name is not None:
            try:
                with open(self.temp_path+name):
                    pass
            except IOError:
                raise ImageUploaderError("Несуществующий файл: " + name)
                return None
            with wand.image.Image(filename=(self.temp_path
                                            + name)) as original:
                with original.clone() as img:
                    img.save(file=img_file)
        elif permanent:
            with wand.image.Image(blob=self.blob) as img:
                img.save(file=img_file)
        else:
            with wand.image.Image(blob=self.blob) as original:
                with original.convert(self.img_format) as img:
                    img.save(file=img_file)
        filename = "/{}{}".format(d_dir,
                                  img_file.name.split("/").pop())
        img_file.close()
        return filename

    def crop(self, info):
        """
        Обрезает изображение по заданным параметрам. Записывает в self.blob
        измененное изображение
        Параметры:
        info = {"l":отступ слева, "t":отступ сверху, "w":ширина кропа, "h":высота кропа}
        """
        with wand.image.Image(blob=self.blob) as img:
            try:
                img.crop(info["l"], info["t"],
                         width=info["w"], height=info["h"])
            except KeyError:
                raise ImageUploaderError("Неправильные пар-мы для кропа")
                return False
            else:
                self.blob = img.make_blob(self.img_format)
                return True

    def validate_as_avatar(self, image):
        """
        Валидация на хард-кодед параметры аватара. Генерирует исключение ImageUploaderError,
        если картинка невалидна
        """
        if (img.height or img.width) > 1500:
            raise ImageUploaderError("Картинка слишком большая")
        elif img.height < 150 or img.width < 300:
            raise ImageUploaderError("Картинка слишком маленькая")

    def resize(self, size):
        """
        Изменяет изображение пропорционально по ширине. True - если оно было изменено,
        False в противном случае
        """
        img = wand.image.Image(blob=self.blob)
        if img.width > size:
            img.sample(size, int((img.height / img.width) * size))
            self.blob = img.make_blob(self.img_format)
            return True
        return False
