import unittest
import time
from bson.objectid import ObjectId
import tornado.escape

class SenderError(Exception):pass
class MessageSender(object):
    def __init__(self, c_user, mongo):
        assert type(c_user) != str, "c_user - это объект, а не строка"
        self.mongo = mongo
        self.c_user = c_user

    def send_message(self, msg):
        """
        Отправляет сообщение пользователю. В случае удачи возвращает True,
        в случае ошибки - False. Это абстрактный слой над классом Dialog, нужен
        для валидации сообщения
        """
        try:
            self._validate(msg)
        except (TypeError, KeyError):
            raise SenderError("Неправильное тело сообщения")
            return False
        except AssertionError:
            raise SenderError("Нужно выбрать друзей")
            return False
        try:
            if set(self.c_user["friends"]).intersection(msg["receivers"]) != msg["receivers"]:
                raise SenderError("Вы не в списке друзей получателя")
        except Exception as e:
            raise SenderError(str(e))
            return False
        else:
            msg["receivers"] = list(msg["receivers"])
            msg["receivers"].append(self.c_user["username"])
            msg["content"] = tornado.escape.xhtml_escape(msg["content"])
            dialog = Dialog(self.c_user, msg["receivers"], self.mongo)
            data = dialog.push(msg)
            if data is not None:
                return data
        return None

    def _validate(self, msg):
        """
        Проверяет тело сообщения на ошибки. Порождает исключения в случае ошибок
        """
        msg["receivers"] = set(msg["receivers"])
        msg["content"] = str(msg["content"])
        assert len(msg["receivers"]) > 0, "Нужно выбрать друзей"
        assert msg["content"], "Пустое сообщение нельзя отправить"

class Dialog(object):
    id  = None

    def __init__(self, c_user, members, mongo):
        assert type(members) == list, "members must be list"
        self.c_user = c_user
        self.mongo = mongo
        self.members = members
        if not self._exist():
            self._add()

    def _exist(self):
        """
        Проверяет сущестование диалога. True - если существует, False - если нет
        """
        dialog = self.mongo.users.dialogs.find_one({"members":{"$all":self.members}})
        if dialog is not None:
            self.id = str(dialog["_id"])
            return True
        return False

    def _add(self):
        """
        Добавляет диалог. Ничего не возвращает. Устанавливает id
        """
        self.id = str(self.mongo.users.dialogs.insert({"members":self.members}))

    def push(self, msg):
        """
        Отправляет сообщение в диалог. True - удачно, False - нет
        """
        c_time = time.strftime("%d.%m.%y - %H:%M", time.localtime())
        msg_data = dict(
                dialog_id=self.id,
                msg_author=self.c_user["username"],
                msg_content=msg["content"],
                date=c_time
                )
        msg_id = self.mongo.users.messages.insert(msg_data)
        if msg_id is not None:
            self.mongo.users.dialogs.update({"_id":ObjectId(self.id)},
                                            {"$set":{
                                            "last":ObjectId(msg_id),
                                            "date":c_time
                                            }})
            return msg_data
        else:
            return None