import re

class TextParser(object):

    _parsers = dict(
        link=(r'\[[a-zа-яA-ZА-Я _\-()]+\]\(.*\)', '<a href="{1}">{0}</a>'),
        user=(r'(?<!>)@[a-z_0-9]{3,}', '<a href="://sclhub.com/{0}">@{0}</a>'),
        tag=(r'(?<!>)#[a-zA-Zа-яА-Я_0-9]{3,}', '<a href="://sclhub.com/tag/{0}">#{0}</a>'),
        )

    def parse(self, text):
        for pattern_name, pattern in self._parsers.items():
            replacements = []
            r = re.compile(pattern[0], flags=re.IGNORECASE)
            repls = r.finditer(text)
            for r in repls:
                replacements.append(r.group())
            count = 0
            while count != len(replacements):
                if pattern_name in ("user", "tag"):
                    text = re.sub(pattern[0], pattern[1].format(replacements[count][1:]), text, count=1)
                elif pattern_name == "link":
                    formatted = "".join(replacements[count][1:-1]).split("](")
                    text = re.sub(pattern[0], pattern[1].format(*formatted), text, count=1)
                count += 1
            del r
        return text