class FormValidationError(Exception): pass
class EmailAlreadyInUseError(Exception): pass
class UsernameAlreadyInUseError(Exception): pass

class FormValidator(object):
    validators = {
        "email": "^[a-z0-9\.]{1,50}@[a-z0-9\.]{1,30}\.[a-z]{2,4}$",
        "full_name": "^[A-ZА-Я]{1}[a-zа-я]{1,50} [A-ZА-Я]{1}[a-zа-я]{1,50}$",
        "username": "^[a-zA-Z_]{3,}$",
        "password": "^.{6,30}$"
    }
    def validate(self, str, pattern_name, ignore_case=1):
        try:
            data = re.match(self.validators[pattern_name], str,
                re.IGNORECASE if ignore_case else 0)
        except KeyError:
            print(pattern_name + " isn't exist in validators")
        except NameError:
            print("'Re' module isn't defined")
        finally:
            if data is not None:
                return True
            raise FormValidationError(pattern_name);
            return False