import time
from bson import ObjectId
import tornado.escape
import sclhub.tools.user

#Для тестов
# import tools.user


class PostError(Exception):
    pass


class Post(object):
    redis = None
    mongo = None
    user = None
    c_user = None
    _post = dict()
    _c_post = None

    def __init__(
        self, pid=None, mongo=None, post=dict(content=None, owner=None),
            c_user=None, redis=None):
        self._post = post
        self.c_user = c_user
        self.current_user = (c_user.profile["username"]
            if c_user is not None else None)
        self.redis = redis
        self.mongo = mongo
        # Должен быть последним
        self.current = pid

    @property
    def current(self):
        return self._c_post

    @current.setter
    def current(self, pid):
        if pid is None:
            if not self.valid():
                raise PostError(
                    "Пост не должен быть пустым, состоять только из пробелов\
                    или быть больше, чем 1080 знаков.")
                return
            if not self.allowed():
                raise PostError(
                    "Нельзя добавить пост к пользователю,\
                    который у вас не в друзьях")
                return
            npid = self.mongo.users.walls.insert(dict(
                owner=(self._post["owner"]).profile["username"],
                content=tornado.escape.xhtml_escape(self._post["content"]),
                date=time.strftime("%d.%m.%y - %H:%M", time.localtime()),
                author=(self.c_user).profile["username"],
                likes=[],
                reblogs=[],
            ))
            if npid is None:
                raise PostError("Проблема на сервере, попробуйте еще раз")
            else:
                self._c_post = self.mongo.users.walls.find_one(
                    {"_id": ObjectId(npid)})

        else:
            post = self.mongo.users.walls.find_one({"_id": ObjectId(pid)})
            if post is None:
                raise PostError("Пост с id = {} не найден".format(pid))
            self._c_post = post
        author = sclhub.tools.user.User(self._c_post["author"], self.redis, self.mongo)
        self._c_post["pic"] = author.profile["pic"]

    def valid(self):
        """
        Проверка на пустоту и количество знаков <= 1080.
        """
        plen = len(self._post["content"])
        if (self._post["content"].isspace() or
                plen == 0 or plen > 1080):
            return False
        return True

    def allowed(self):
        """
        Проверка на возможность добавления по пользователю
        """
        if (self.c_user.profile["username"] ==
                (self._post["owner"]).profile["username"]):
            return True
        if (self.c_user.profile["username"]
                in (self._post["owner"]).profile["friends"]):
            return True
        return False

    def delete(self):
        """
        Удаляет пост. Ничего не возвращает
        """
        current_user = (self.c_user).profile["username"]
        if (self.by(current_user) or self.by(current_user, True)) is not None:
            if (self.mongo.users.walls.remove(
                    {"_id": ObjectId(self.current["_id"])}) is None):
                raise PostError("Проблема на сервере. Попробуйте еще раз")
            else:
                return
        raise PostError("Нельзя удалить чужую запись")

    def by(self, username=None, wall=False):
        """
        Если username не задано, определяет, является ли владельцем записи
        текущий пользователь. Иначе возвращает username владельца.
        Если wall=True, тогда выясняется, принадлежит ли запись стене юзера
        """
        poss = "owner" if wall else "author"
        if username is None:
            return self.mongo.users.walls.find_one(
                    {"_id": ObjectId(self.current["_id"])})[poss]
        return (self.mongo.users.walls.find_one({"_id":
            ObjectId(self.current["_id"])})[poss] == username)

    def comment(self, com):
        if self.allowed():
            copt = dict(
                pid=self.current["_id"],
                current_user=self.current_user,
                content=com,
                mongo=self.mongo
                )
            comment = Comment(**copt)
            return comment.current
        raise CommentError("Нельзя комментировать записи, если пользователь"\
            " не добавлен в друзья")

    def uncomment(self, cid):
        comment = Comment(cid=cid, mongo=self.mongo)
        if (self.by(self.current_user, True) or 
            comment.current["author"] == self.current_user):
            comment.delete()

    def load_comments(self, start=0, lim=5, sort_by="date", sort=1):
        coms = self.mongo.users.comments.find({"pid": self.current["_id"]}).skip(
            int(start)).limit(int(lim)).sort(sort_by, sort)
        comments = []
        for com in coms:
            com["_id"] = str(com["_id"])
            com["pid"] = str(com["pid"])
            comments.append(com)
        return comments


    def like(self):
        """
        Добаляет лайк к записи. Возвращает 1, при добавлении. При удалении
        возвращает -1
        """
        if (self.c_user).profile["username"] in self.current["likes"]:
            self.mongo.users.walls.update(
                {"_id":ObjectId(self.current["_id"])},
                {"$pull":{"likes":self.c_user.profile["username"]}})
            return -1
        self.mongo.users.walls.update(
                        {"_id":ObjectId(self.current["_id"])},
                        {"$push":{"likes":(self.c_user).profile["username"]}})
        return 1

    def reblog(self):
        """
        Делает реблог записи. Возвращает id'шник записи при реблоге и None при 
        отмене реблога
        """
        print("--------------")
        print(self.current)
        if self.by(self.current_user, True) and self.by(self.current_user):
            raise PostError("Нелья сделать реблог своей"\
                " записи на своей же стене")
            return
        if self.current_user in self.current["reblogs"]:
            self.mongo.users.walls.update(
                {"_id":ObjectId(self.current["_id"])},
                {"$pull":{"reblogs":self.current_user}})
            self.mongo.users.walls.remove(
                {"origin":ObjectId(self.current["_id"]), "owner":self.current_user})
            #TODO уменьшить ранк пользователя
            return None
        self.mongo.users.walls.update(
                        {"_id":ObjectId(self.current["_id"])},
                        {"$push":{"reblogs":self.current_user}})
        reblogged = dict(
            author=self.current["author"],
            owner=self.current_user,
            content=self.current["content"],
            origin=self.current["_id"],
            likes=[],
            reblogs=[],
            date=time.strftime("%d.%m.%y - %H:%M", time.localtime()),
            )
        #TODO увеличить ранк пользователя
        npid = self.mongo.users.walls.insert(reblogged)
        if npid is None:
            raise PostError("Сбой на сервере. Попробуйте еще раз")
        return npid

class CommentError(Exception):
    pass


class Comment(object):
    content = None
    current_user = None
    _c_com = None

    def __init__(self, current_user=None, mongo=None, pid=None, cid=None, content=None):
        self.content = content
        self.current_user = current_user
        self.mongo = mongo
        self.pid = pid
        self.current = cid

    @property
    def current(self):
        return self._c_com

    @current.setter
    def current(self, cid):
        if cid is None:
            if not self.valid():
                raise CommentError("Комментарий не должен быть пустым или"\
                " превышать 180 символов")
                return
            #Так делать нельзя. I'm bad
            author = sclhub.tools.user.User(self.current_user, None, self.mongo)
            ncid = self.mongo.users.comments.insert(dict(
                pid=self.pid,
                date=time.strftime("%d.%m.%y - %H:%M", time.localtime()),
                content=tornado.escape.xhtml_escape(self.content),
                author=author.profile["username"],
                ))
            if ncid is None:
                raise CommentError("Ошибка сервера. Попробуйте еще раз")
            else:
                self._c_com = self.mongo.users.comments.find_one(
                    {"_id": ObjectId(ncid)})
        else:
            com = self.mongo.users.comments.find_one({"_id": ObjectId(cid)})
            if com is None:
                raise CommentError("Комментарий с id = {} не найден".format(cid))
            self._c_com = com
        self._c_com["pic"] = author.profile["pic"]

    def valid(self):
        """
        Проверка на пустоту, количество знаков <= 180
        """
        comlen = len(self.content)
        if (self.content.isspace() or comlen == 0 or comlen > 180):
            return False
        return True

    def delete(self):
        """
        Удаляет комментарий
        """
        if self.mongo.users.comments.remove({"_id":self.current["_id"]}) is None:
            raise CommentError("Ошибка удаления. Попробуйте еще раз")

class Tape(object):

    redis = None
    mongo = None
    limit = 7
    sort = [("date", -1), ("_id", -1)]

    def __init__(self, user, redis, mongo):
        assert type(user)!=str, "user - объект"
        self.user = user
        self.redis = redis
        self.mongo = mongo

    def load(self, owners):
        self._posts = []
        cursor = self.mongo.users.walls.find({"owner":{"$in":owners}}).limit(self.limit).sort(self.sort)
        for post in cursor:
            post["pic"] = sclhub.tools.user.User(post["author"], None, self.mongo).profile["pic"]
            self._posts.append(post)

    def init_comments(self, skip=0, lim=4):
        for post in self._posts:
            com_cursor = self.mongo.users.comments.find({"pid":post['_id']}).skip(skip).sort("date", 1).limit(lim)
            com_count = self.mongo.users.comments.find({"pid":post['_id']}).count()
            com_count = 0 if com_count is None else com_count
            comments = []
            for com in com_cursor:
                com["pic"] = sclhub.tools.user.User(com["author"], None, self.mongo).profile["pic"]
                comments.append(com)
            post["comments"] = comments
            post["com_count"] = com_count

    def get(self):
        return self._posts

    def count(self):
        return self.mongo.users.walls.find({"owner":(self.user).profile["username"]}).count()