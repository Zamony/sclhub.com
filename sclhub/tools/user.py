import re


class UserError(Exception):
    pass


class User(object):
    profile = None
    validators = {
        "email": "^[a-z0-9\.]{1,50}@[a-z0-9\.]{1,30}\.[a-z]{2,4}$",
        "full_name": "([A-Z]{1}[a-z]{1,50}\ [A-Z]{1}[a-z]{1,50})|([А-Я]{1}[а-я]{1,50}\ [А-Я]{1}[а-я]{1,50})",
        "password": "^.{6,30}$"
    }

    def __init__(self, username, redis, mongo):
        user = mongo.users.profiles.find_one({"username": username})
        if user is not None:
            self.profile = user
            self.redis = redis
            self.mongo = mongo
        else:
            raise UserError("Пользователя " + username + " нет")

    @property
    def online_friends(self):
        pipe = self.redis.pipeline()
        for f in self.profile["friends"]:
            pipe.sadd("friends." + self.profile["username"], f)
        pipe.zinterstore("online." + self.profile["username"],
                         ["friends." + self.profile["username"], "users.online"])
        pipe.delete("friends." + self.profile["username"])
        pipe.execute()
        online = self.redis.zrange("online." + self.profile["username"], 0, -1)
        self.redis.delete("online." + self.profile["username"])
        return online

    def update_profile(self, data, secure=True):
        if secure and not self._verify_data(data):
            return False
        return self.mongo.users.profiles.update(
            {"username": self.profile["username"]},
            {
                "$set": data
            })

    def _verify_data(self, data):
        for k, v in data.items():
            print(k,v)
            if re.match(self.validators[k], v) is None:
                raise UserError("Неправильное заполнение полей")
                return False
        return True

class SimpleUser(object):

    redis = None
    mongo = None
    prefix = "user."
    internal_keys = ("full_name", "avatar", "pic")

    _bucket = dict()
    _last_user_username = None
    _last_user_data = dict()

    def __init__(self, redis, mongo):
        self.redis = redis
        self.mongo = mongo

    def get(self, username):
        try:
            booked = self._bucket[username]
        except KeyError:
            key = self.prefix + username
            if self.redis.exists(key):
                self._last_user_data = dict(zip(self.internal_keys, (i.decode("utf-8") for i in self.redis.hmget(key, self.internal_keys))))
                self._last_user_username = username
                return self._last_user_data
            user = self.mongo.users.profiles.find_one({"username":username})
            if user is not None:
                simple_user = {i:user[i] for i in self.internal_keys}
                self.redis.hmset(key, simple_user)
                return simple_user
            raise UserError("Пользователь {} не найден".format(username))
        else:
            return booked

    def book(self):
        if self._last_user_username not in self._bucket.keys():
            self._bucket[self._last_user_username] = self._last_user_data
            return True
        return False