import smtplib
import json
import uuid
import email.mime.multipart
import email.mime.text
import tornado.escape

class RegisterManager(object):

    r_prefix = "register."

    def __init__(self, redis, mongo):
        self.redis = redis
        self.mongo = mongo

    def confirm(self, code):
        t_user = self.redis.get(self.r_prefix + code).decode("utf-8")
        if t_user is not None:
            # print(t_user)
            r_data = json.loads(t_user)
            self.redis.delete(self.r_prefix + code)
            self.redis.srem(self.r_prefix + "emails", r_data["email"])
            self.add_user(r_data)
            return True
        return False

    def send_confirmation(self, profile):
        msg = email.mime.multipart.MIMEMultipart('alternative')
        link = "sclhub.com/confirm?code=" + tornado.escape.xhtml_escape(profile["code"])
        msg['From'] = 'SclHub.com'
        msg['To'] = profile["email"]
        msg['Subject'] = 'Подтверждение регистрации на SclHub.com'
        message = """<!doctype html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <title>Подтверждение регистрации</title>
                    </head>
                    <body>
                    <p>Учетная запись с именем {0}(@{1}) создана. Пройдите по следующей
        ссылке, чтобы стать полноправным участником сети:</p>
        <a href="http://{2}">{2}</a>
                    </body>
                    </html>
                    """
        message = message.format(profile["full_name"], profile["username"], link)
        msg.attach(email.mime.text.MIMEText(message, "html"))
        mailserver = smtplib.SMTP('localhost')
        mailserver.sendmail('SclHub.com', profile["email"], msg.as_string())
        mailserver.quit()

    def add_user(self, profile):
        profile["avatar"] = "/static/img/default_nebula.jpeg"
        profile["pic"] = "/static/img/default_nebula_small.jpeg"
        profile["friends"] = []
        self.mongo.users.profiles.insert(profile)

    def create_temp_user(self, username, password, full_name, email):
        if not self.redis.sismember(self.r_prefix + "emails", email):
            u_opt = dict(
                username=username,
                password=password,
                email=email,
                full_name=full_name,
                )
            code = str(uuid.uuid4())
            #259200 - трое суток
            self.redis.setex(self.r_prefix + code, 259200, json.dumps(u_opt))
            self.redis.sadd(self.r_prefix + "emails", email)
            return code
        else:
            return None