import time
import tornado.web


class RedisRequestHandler(tornado.web.RequestHandler):
    redis = None
    _argums = None

    def initialize(self, redis, loader=None, mongo=None):
        self.redis = redis

    def get_current_user(self):
        try:
            username = self.redis.get("session." + self.get_cookie("session_data")).decode("utf-8")
        except (AttributeError, TypeError):
            username = None
        else:
            # Статус онлайн живет 3 минуты, но сборщик удалит его несколько позже
            self.redis.zadd("users.online", int(time.time()) + 60 * 3, username)
        return username

    @property
    def arguments(self):
        if self._argums is None:
            self._argums = dict((k, self.decode_argument(arg[0]))
                                for (k, arg) in self.request.arguments.items())
        return self._argums
